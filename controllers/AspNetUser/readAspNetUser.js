const { QueryTypes } = require("sequelize");
const { sequelize } = require("../../models");

module.exports = async (req, res) => {
  try {
    const { date } = req.params;
    console.log("++++++++++++++++++++++++++", date);
    const readAspNetUser = await sequelize.query(
      `
    SELECT anu."Id", anu."UserName", anu."NormalizedUserName", 
    anu."Email", anu."NormalizedEmail", anu."EmailConfirmed", 
    anu."PasswordHash", anu."SecurityStamp", anu."ConcurrencyStamp", 
    anu."PhoneNumber", anu."PhoneNumberConfirmed", anu."TwoFactorEnabled", 
    anu."LockoutEnd", anu."LockoutEnabled", anu."AccessFailedCount", 
    anu."FirstName", anu."Surname", anu."Gender", 
    anu."Birthdate", anu."JoinTime", anu."Status", anu."Nationality", 
    anu."IdentityCardNumber", anu."IdentityCardExpiryDate", anu."TaxIdentificationNumber", 
    anu."TaxIdentificationNumberExpiryDate", anu."ReferralCode", anu."InvitedBy", 
    anu."ImageAttachmentUri", anu."IdentityCardAttachmentUri", anu."TaxIdentificationNumberAttachmentUri", 
    anu."Pin", anu."BirthPlace", anur."RoleId" as "Role ID", anr."Name" as "Role Name" ,anr."NormalizedName" as "Role NormalizedName" ,anr."ConcurrencyStamp" as "Role ConcurrencyStamp"
    FROM "AspNetUsers" anu 
    inner join "AspNetUserRoles" anur on anur."UserId" = anu."Id" 
    inner join "AspNetRoles" anr on anr."Id" = anur."RoleId" 
    where "JoinTime" between '${date} 00:00:01' and '${date} 11:59:59';
        `
    );
    res.status(200).json(readAspNetUser);
  } catch (error) {
    console.log(error);
  }
};
