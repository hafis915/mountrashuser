const route = require("express").Router()
const {aspNetUserController} = require("../controllers/index")

route.get("/user/:date", aspNetUserController.readAspNetUser)

module.exports = route