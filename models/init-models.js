var DataTypes = require("sequelize").DataTypes;
var _ApiClaims = require("./ApiClaims");
var _ApiProperties = require("./ApiProperties");
var _ApiResources = require("./ApiResources");
var _ApiScopeClaims = require("./ApiScopeClaims");
var _ApiScopes = require("./ApiScopes");
var _ApiSecrets = require("./ApiSecrets");
var _AspNetRoleClaims = require("./AspNetRoleClaims");
var _AspNetRoles = require("./AspNetRoles");
var _AspNetUserClaims = require("./AspNetUserClaims");
var _AspNetUserLogins = require("./AspNetUserLogins");
var _AspNetUserRoles = require("./AspNetUserRoles");
var _AspNetUserRolesTemp = require("./AspNetUserRolesTemp");
var _AspNetUserTokens = require("./AspNetUserTokens");
var _AspNetUsers = require("./AspNetUsers");
var _AspNetUsersTemp = require("./AspNetUsersTemp");
var _ClientClaims = require("./ClientClaims");
var _ClientCorsOrigins = require("./ClientCorsOrigins");
var _ClientGrantTypes = require("./ClientGrantTypes");
var _ClientIdPRestrictions = require("./ClientIdPRestrictions");
var _ClientPostLogoutRedirectUris = require("./ClientPostLogoutRedirectUris");
var _ClientProperties = require("./ClientProperties");
var _ClientRedirectUris = require("./ClientRedirectUris");
var _ClientScopes = require("./ClientScopes");
var _ClientSecrets = require("./ClientSecrets");
var _Clients = require("./Clients");
var _DeviceCodes = require("./DeviceCodes");
var _IdentityClaims = require("./IdentityClaims");
var _IdentityProperties = require("./IdentityProperties");
var _IdentityResources = require("./IdentityResources");
var _PersistedGrants = require("./PersistedGrants");
var ___EFMigrationsHistory = require("./__EFMigrationsHistory");

function initModels(sequelize) {
  var ApiClaims = _ApiClaims(sequelize, DataTypes);
  var ApiProperties = _ApiProperties(sequelize, DataTypes);
  var ApiResources = _ApiResources(sequelize, DataTypes);
  var ApiScopeClaims = _ApiScopeClaims(sequelize, DataTypes);
  var ApiScopes = _ApiScopes(sequelize, DataTypes);
  var ApiSecrets = _ApiSecrets(sequelize, DataTypes);
  var AspNetRoleClaims = _AspNetRoleClaims(sequelize, DataTypes);
  var AspNetRoles = _AspNetRoles(sequelize, DataTypes);
  var AspNetUserClaims = _AspNetUserClaims(sequelize, DataTypes);
  var AspNetUserLogins = _AspNetUserLogins(sequelize, DataTypes);
  var AspNetUserRoles = _AspNetUserRoles(sequelize, DataTypes);
  var AspNetUserRolesTemp = _AspNetUserRolesTemp(sequelize, DataTypes);
  var AspNetUserTokens = _AspNetUserTokens(sequelize, DataTypes);
  var AspNetUsers = _AspNetUsers(sequelize, DataTypes);
  var AspNetUsersTemp = _AspNetUsersTemp(sequelize, DataTypes);
  var ClientClaims = _ClientClaims(sequelize, DataTypes);
  var ClientCorsOrigins = _ClientCorsOrigins(sequelize, DataTypes);
  var ClientGrantTypes = _ClientGrantTypes(sequelize, DataTypes);
  var ClientIdPRestrictions = _ClientIdPRestrictions(sequelize, DataTypes);
  var ClientPostLogoutRedirectUris = _ClientPostLogoutRedirectUris(sequelize, DataTypes);
  var ClientProperties = _ClientProperties(sequelize, DataTypes);
  var ClientRedirectUris = _ClientRedirectUris(sequelize, DataTypes);
  var ClientScopes = _ClientScopes(sequelize, DataTypes);
  var ClientSecrets = _ClientSecrets(sequelize, DataTypes);
  var Clients = _Clients(sequelize, DataTypes);
  var DeviceCodes = _DeviceCodes(sequelize, DataTypes);
  var IdentityClaims = _IdentityClaims(sequelize, DataTypes);
  var IdentityProperties = _IdentityProperties(sequelize, DataTypes);
  var IdentityResources = _IdentityResources(sequelize, DataTypes);
  var PersistedGrants = _PersistedGrants(sequelize, DataTypes);
  var __EFMigrationsHistory = ___EFMigrationsHistory(sequelize, DataTypes);

  AspNetRoles.belongsToMany(AspNetUsers, { as: 'Users', through: AspNetUserRoles, foreignKey: "RoleId", otherKey: "UserId" });
  AspNetUsers.belongsToMany(AspNetRoles, { as: 'Roles', through: AspNetUserRoles, foreignKey: "UserId", otherKey: "RoleId" });
  ApiClaims.belongsTo(ApiResources, { as: "ApiResource", foreignKey: "ApiResourceId"});
  ApiResources.hasMany(ApiClaims, { as: "ApiClaims", foreignKey: "ApiResourceId"});
  ApiProperties.belongsTo(ApiResources, { as: "ApiResource", foreignKey: "ApiResourceId"});
  ApiResources.hasMany(ApiProperties, { as: "ApiProperties", foreignKey: "ApiResourceId"});
  ApiScopes.belongsTo(ApiResources, { as: "ApiResource", foreignKey: "ApiResourceId"});
  ApiResources.hasMany(ApiScopes, { as: "ApiScopes", foreignKey: "ApiResourceId"});
  ApiSecrets.belongsTo(ApiResources, { as: "ApiResource", foreignKey: "ApiResourceId"});
  ApiResources.hasMany(ApiSecrets, { as: "ApiSecrets", foreignKey: "ApiResourceId"});
  ApiScopeClaims.belongsTo(ApiScopes, { as: "ApiScope", foreignKey: "ApiScopeId"});
  ApiScopes.hasMany(ApiScopeClaims, { as: "ApiScopeClaims", foreignKey: "ApiScopeId"});
  AspNetRoleClaims.belongsTo(AspNetRoles, { as: "Role", foreignKey: "RoleId"});
  AspNetRoles.hasMany(AspNetRoleClaims, { as: "AspNetRoleClaims", foreignKey: "RoleId"});
  AspNetUserRoles.belongsTo(AspNetRoles, { as: "Role", foreignKey: "RoleId"});
  AspNetRoles.hasMany(AspNetUserRoles, { as: "AspNetUserRoles", foreignKey: "RoleId"});
  AspNetUserClaims.belongsTo(AspNetUsers, { as: "User", foreignKey: "UserId"});
  AspNetUsers.hasMany(AspNetUserClaims, { as: "AspNetUserClaims", foreignKey: "UserId"});
  AspNetUserLogins.belongsTo(AspNetUsers, { as: "User", foreignKey: "UserId"});
  AspNetUsers.hasMany(AspNetUserLogins, { as: "AspNetUserLogins", foreignKey: "UserId"});
  AspNetUserRoles.belongsTo(AspNetUsers, { as: "User", foreignKey: "UserId"});
  AspNetUsers.hasMany(AspNetUserRoles, { as: "AspNetUserRoles", foreignKey: "UserId"});
  AspNetUserTokens.belongsTo(AspNetUsers, { as: "User", foreignKey: "UserId"});
  AspNetUsers.hasMany(AspNetUserTokens, { as: "AspNetUserTokens", foreignKey: "UserId"});
  ClientClaims.belongsTo(Clients, { as: "Client", foreignKey: "ClientId"});
  Clients.hasMany(ClientClaims, { as: "ClientClaims", foreignKey: "ClientId"});
  ClientCorsOrigins.belongsTo(Clients, { as: "Client", foreignKey: "ClientId"});
  Clients.hasMany(ClientCorsOrigins, { as: "ClientCorsOrigins", foreignKey: "ClientId"});
  ClientGrantTypes.belongsTo(Clients, { as: "Client", foreignKey: "ClientId"});
  Clients.hasMany(ClientGrantTypes, { as: "ClientGrantTypes", foreignKey: "ClientId"});
  ClientIdPRestrictions.belongsTo(Clients, { as: "Client", foreignKey: "ClientId"});
  Clients.hasMany(ClientIdPRestrictions, { as: "ClientIdPRestrictions", foreignKey: "ClientId"});
  ClientPostLogoutRedirectUris.belongsTo(Clients, { as: "Client", foreignKey: "ClientId"});
  Clients.hasMany(ClientPostLogoutRedirectUris, { as: "ClientPostLogoutRedirectUris", foreignKey: "ClientId"});
  ClientProperties.belongsTo(Clients, { as: "Client", foreignKey: "ClientId"});
  Clients.hasMany(ClientProperties, { as: "ClientProperties", foreignKey: "ClientId"});
  ClientRedirectUris.belongsTo(Clients, { as: "Client", foreignKey: "ClientId"});
  Clients.hasMany(ClientRedirectUris, { as: "ClientRedirectUris", foreignKey: "ClientId"});
  ClientScopes.belongsTo(Clients, { as: "Client", foreignKey: "ClientId"});
  Clients.hasMany(ClientScopes, { as: "ClientScopes", foreignKey: "ClientId"});
  ClientSecrets.belongsTo(Clients, { as: "Client", foreignKey: "ClientId"});
  Clients.hasMany(ClientSecrets, { as: "ClientSecrets", foreignKey: "ClientId"});
  IdentityClaims.belongsTo(IdentityResources, { as: "IdentityResource", foreignKey: "IdentityResourceId"});
  IdentityResources.hasMany(IdentityClaims, { as: "IdentityClaims", foreignKey: "IdentityResourceId"});
  IdentityProperties.belongsTo(IdentityResources, { as: "IdentityResource", foreignKey: "IdentityResourceId"});
  IdentityResources.hasMany(IdentityProperties, { as: "IdentityProperties", foreignKey: "IdentityResourceId"});

  return {
    ApiClaims,
    ApiProperties,
    ApiResources,
    ApiScopeClaims,
    ApiScopes,
    ApiSecrets,
    AspNetRoleClaims,
    AspNetRoles,
    AspNetUserClaims,
    AspNetUserLogins,
    AspNetUserRoles,
    AspNetUserRolesTemp,
    AspNetUserTokens,
    AspNetUsers,
    AspNetUsersTemp,
    ClientClaims,
    ClientCorsOrigins,
    ClientGrantTypes,
    ClientIdPRestrictions,
    ClientPostLogoutRedirectUris,
    ClientProperties,
    ClientRedirectUris,
    ClientScopes,
    ClientSecrets,
    Clients,
    DeviceCodes,
    IdentityClaims,
    IdentityProperties,
    IdentityResources,
    PersistedGrants,
    __EFMigrationsHistory,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
