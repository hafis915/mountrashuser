const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClientPostLogoutRedirectUris', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    PostLogoutRedirectUri: {
      type: DataTypes.STRING(2000),
      allowNull: false
    },
    ClientId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Clients',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ClientPostLogoutRedirectUris',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ClientPostLogoutRedirectUris_ClientId",
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_ClientPostLogoutRedirectUris",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
