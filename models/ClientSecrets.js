const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClientSecrets', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Description: {
      type: DataTypes.STRING(2000),
      allowNull: true
    },
    Value: {
      type: DataTypes.STRING(4000),
      allowNull: false
    },
    Expiration: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Type: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    Created: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ClientId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Clients',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ClientSecrets',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ClientSecrets_ClientId",
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_ClientSecrets",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
