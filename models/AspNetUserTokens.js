const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('AspNetUserTokens', {
    UserId: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'AspNetUsers',
        key: 'Id'
      }
    },
    LoginProvider: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true
    },
    Name: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true
    },
    Value: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'AspNetUserTokens',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "PK_AspNetUserTokens",
        unique: true,
        fields: [
          { name: "UserId" },
          { name: "LoginProvider" },
          { name: "Name" },
        ]
      },
    ]
  });
};
