const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ApiProperties', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Key: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    Value: {
      type: DataTypes.STRING(2000),
      allowNull: false
    },
    ApiResourceId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'ApiResources',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ApiProperties',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ApiProperties_ApiResourceId",
        fields: [
          { name: "ApiResourceId" },
        ]
      },
      {
        name: "PK_ApiProperties",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
