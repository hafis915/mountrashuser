const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClientProperties', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Key: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    Value: {
      type: DataTypes.STRING(2000),
      allowNull: false
    },
    ClientId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Clients',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ClientProperties',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ClientProperties_ClientId",
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_ClientProperties",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
