const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ApiClaims', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Type: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    ApiResourceId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'ApiResources',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ApiClaims',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ApiClaims_ApiResourceId",
        fields: [
          { name: "ApiResourceId" },
        ]
      },
      {
        name: "PK_ApiClaims",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
