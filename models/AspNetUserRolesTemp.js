const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('AspNetUserRolesTemp', {
    UserId: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true
    },
    RoleId: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true
    }
  }, {
    sequelize,
    tableName: 'AspNetUserRolesTemp',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "AspNetUserRolesTemp_RoleId_idx",
        fields: [
          { name: "RoleId" },
        ]
      },
      {
        name: "AspNetUserRolesTemp_pkey",
        unique: true,
        fields: [
          { name: "UserId" },
          { name: "RoleId" },
        ]
      },
    ]
  });
};
