const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ApiSecrets', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Description: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    Value: {
      type: DataTypes.STRING(4000),
      allowNull: false
    },
    Expiration: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Type: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    Created: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ApiResourceId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'ApiResources',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ApiSecrets',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ApiSecrets_ApiResourceId",
        fields: [
          { name: "ApiResourceId" },
        ]
      },
      {
        name: "PK_ApiSecrets",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
