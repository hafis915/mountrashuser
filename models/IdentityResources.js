const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('IdentityResources', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    Name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    DisplayName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    Description: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    Required: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    Emphasize: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    ShowInDiscoveryDocument: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    Created: {
      type: DataTypes.DATE,
      allowNull: false
    },
    Updated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    NonEditable: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'IdentityResources',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_IdentityResources_Name",
        unique: true,
        fields: [
          { name: "Name" },
        ]
      },
      {
        name: "PK_IdentityResources",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
