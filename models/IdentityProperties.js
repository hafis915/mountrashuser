const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('IdentityProperties', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Key: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    Value: {
      type: DataTypes.STRING(2000),
      allowNull: false
    },
    IdentityResourceId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'IdentityResources',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'IdentityProperties',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_IdentityProperties_IdentityResourceId",
        fields: [
          { name: "IdentityResourceId" },
        ]
      },
      {
        name: "PK_IdentityProperties",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
