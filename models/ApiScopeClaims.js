const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ApiScopeClaims', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Type: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    ApiScopeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'ApiScopes',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ApiScopeClaims',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ApiScopeClaims_ApiScopeId",
        fields: [
          { name: "ApiScopeId" },
        ]
      },
      {
        name: "PK_ApiScopeClaims",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
