const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('PersistedGrants', {
    Key: {
      type: DataTypes.STRING(200),
      allowNull: false,
      primaryKey: true
    },
    Type: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    SubjectId: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    ClientId: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    CreationTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    Expiration: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Data: {
      type: DataTypes.STRING(50000),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'PersistedGrants',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_PersistedGrants_Expiration",
        fields: [
          { name: "Expiration" },
        ]
      },
      {
        name: "IX_PersistedGrants_SubjectId_ClientId_Type",
        fields: [
          { name: "SubjectId" },
          { name: "ClientId" },
          { name: "Type" },
        ]
      },
      {
        name: "PK_PersistedGrants",
        unique: true,
        fields: [
          { name: "Key" },
        ]
      },
    ]
  });
};
