const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('AspNetRoleClaims', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    RoleId: {
      type: DataTypes.TEXT,
      allowNull: false,
      references: {
        model: 'AspNetRoles',
        key: 'Id'
      }
    },
    ClaimType: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ClaimValue: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'AspNetRoleClaims',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_AspNetRoleClaims_RoleId",
        fields: [
          { name: "RoleId" },
        ]
      },
      {
        name: "PK_AspNetRoleClaims",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
