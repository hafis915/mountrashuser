const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('AspNetUsers', {
    Id: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true
    },
    UserName: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    NormalizedUserName: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    Email: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    NormalizedEmail: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    EmailConfirmed: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    PasswordHash: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    SecurityStamp: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ConcurrencyStamp: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    PhoneNumber: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    PhoneNumberConfirmed: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    TwoFactorEnabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    LockoutEnd: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LockoutEnabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    AccessFailedCount: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    FirstName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    Surname: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    Gender: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Birthdate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    JoinTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    Status: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    Nationality: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    IdentityCardNumber: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    IdentityCardExpiryDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    TaxIdentificationNumber: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    TaxIdentificationNumberExpiryDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ReferralCode: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    InvitedBy: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ImageAttachmentUri: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    IdentityCardAttachmentUri: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    TaxIdentificationNumberAttachmentUri: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Pin: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    BirthPlace: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'AspNetUsers',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "EmailIndex",
        fields: [
          { name: "NormalizedEmail" },
        ]
      },
      {
        name: "IX_AspNetUsers_Email",
        fields: [
          { name: "Email" },
        ]
      },
      {
        name: "IX_AspNetUsers_PhoneNumber",
        unique: true,
        fields: [
          { name: "PhoneNumber" },
        ]
      },
      {
        name: "IX_AspNetUsers_ReferralCode",
        unique: true,
        fields: [
          { name: "ReferralCode" },
        ]
      },
      {
        name: "IX_AspNetUsers_UserName",
        unique: true,
        fields: [
          { name: "UserName" },
        ]
      },
      {
        name: "PK_AspNetUsers",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
      {
        name: "UserNameIndex",
        unique: true,
        fields: [
          { name: "NormalizedUserName" },
        ]
      },
    ]
  });
};
