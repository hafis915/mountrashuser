const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClientIdPRestrictions', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Provider: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    ClientId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Clients',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ClientIdPRestrictions',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ClientIdPRestrictions_ClientId",
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_ClientIdPRestrictions",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
