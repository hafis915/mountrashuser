const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('IdentityClaims', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Type: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    IdentityResourceId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'IdentityResources',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'IdentityClaims',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_IdentityClaims_IdentityResourceId",
        fields: [
          { name: "IdentityResourceId" },
        ]
      },
      {
        name: "PK_IdentityClaims",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
