const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClientClaims', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Type: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    Value: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    ClientId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Clients',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ClientClaims',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ClientClaims_ClientId",
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_ClientClaims",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
