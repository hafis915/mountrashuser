const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('DeviceCodes', {
    UserCode: {
      type: DataTypes.STRING(200),
      allowNull: false,
      primaryKey: true
    },
    DeviceCode: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    SubjectId: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    ClientId: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    CreationTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    Expiration: {
      type: DataTypes.DATE,
      allowNull: false
    },
    Data: {
      type: DataTypes.STRING(50000),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'DeviceCodes',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_DeviceCodes_DeviceCode",
        unique: true,
        fields: [
          { name: "DeviceCode" },
        ]
      },
      {
        name: "IX_DeviceCodes_Expiration",
        fields: [
          { name: "Expiration" },
        ]
      },
      {
        name: "PK_DeviceCodes",
        unique: true,
        fields: [
          { name: "UserCode" },
        ]
      },
    ]
  });
};
