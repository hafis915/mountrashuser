const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Clients', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    ClientId: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    ProtocolType: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    RequireClientSecret: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    ClientName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    Description: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    ClientUri: {
      type: DataTypes.STRING(2000),
      allowNull: true
    },
    LogoUri: {
      type: DataTypes.STRING(2000),
      allowNull: true
    },
    RequireConsent: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    AllowRememberConsent: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    AlwaysIncludeUserClaimsInIdToken: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    RequirePkce: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    AllowPlainTextPkce: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    AllowAccessTokensViaBrowser: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    FrontChannelLogoutUri: {
      type: DataTypes.STRING(2000),
      allowNull: true
    },
    FrontChannelLogoutSessionRequired: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    BackChannelLogoutUri: {
      type: DataTypes.STRING(2000),
      allowNull: true
    },
    BackChannelLogoutSessionRequired: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    AllowOfflineAccess: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    IdentityTokenLifetime: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    AccessTokenLifetime: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    AuthorizationCodeLifetime: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ConsentLifetime: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    AbsoluteRefreshTokenLifetime: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    SlidingRefreshTokenLifetime: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    RefreshTokenUsage: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    UpdateAccessTokenClaimsOnRefresh: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    RefreshTokenExpiration: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    AccessTokenType: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    EnableLocalLogin: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    IncludeJwtId: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    AlwaysSendClientClaims: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    ClientClaimsPrefix: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    PairWiseSubjectSalt: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    Created: {
      type: DataTypes.DATE,
      allowNull: false
    },
    Updated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LastAccessed: {
      type: DataTypes.DATE,
      allowNull: true
    },
    UserSsoLifetime: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    UserCodeType: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DeviceCodeLifetime: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    NonEditable: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'Clients',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_Clients_ClientId",
        unique: true,
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_Clients",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
