const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ApiScopes', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    DisplayName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    Description: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    Required: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    Emphasize: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    ShowInDiscoveryDocument: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    ApiResourceId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'ApiResources',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ApiScopes',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ApiScopes_ApiResourceId",
        fields: [
          { name: "ApiResourceId" },
        ]
      },
      {
        name: "IX_ApiScopes_Name",
        unique: true,
        fields: [
          { name: "Name" },
        ]
      },
      {
        name: "PK_ApiScopes",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
