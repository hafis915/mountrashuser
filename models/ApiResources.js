const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ApiResources', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    Name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    DisplayName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    Description: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    Created: {
      type: DataTypes.DATE,
      allowNull: false
    },
    Updated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LastAccessed: {
      type: DataTypes.DATE,
      allowNull: true
    },
    NonEditable: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'ApiResources',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ApiResources_Name",
        unique: true,
        fields: [
          { name: "Name" },
        ]
      },
      {
        name: "PK_ApiResources",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
