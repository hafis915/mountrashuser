const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClientGrantTypes', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    GrantType: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    ClientId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Clients',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ClientGrantTypes',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ClientGrantTypes_ClientId",
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_ClientGrantTypes",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
