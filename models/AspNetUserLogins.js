const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('AspNetUserLogins', {
    LoginProvider: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true
    },
    ProviderKey: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true
    },
    ProviderDisplayName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    UserId: {
      type: DataTypes.TEXT,
      allowNull: false,
      references: {
        model: 'AspNetUsers',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'AspNetUserLogins',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_AspNetUserLogins_UserId",
        fields: [
          { name: "UserId" },
        ]
      },
      {
        name: "PK_AspNetUserLogins",
        unique: true,
        fields: [
          { name: "LoginProvider" },
          { name: "ProviderKey" },
        ]
      },
    ]
  });
};
