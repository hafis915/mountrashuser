const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClientCorsOrigins', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Origin: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    ClientId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Clients',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ClientCorsOrigins',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ClientCorsOrigins_ClientId",
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_ClientCorsOrigins",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
