const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClientScopes', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Scope: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    ClientId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Clients',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ClientScopes',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ClientScopes_ClientId",
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_ClientScopes",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
