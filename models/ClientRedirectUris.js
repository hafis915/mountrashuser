const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClientRedirectUris', {
    Id: {
      autoIncrement: true,
      autoIncrementIdentity: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    RedirectUri: {
      type: DataTypes.STRING(2000),
      allowNull: false
    },
    ClientId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Clients',
        key: 'Id'
      }
    }
  }, {
    sequelize,
    tableName: 'ClientRedirectUris',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "IX_ClientRedirectUris_ClientId",
        fields: [
          { name: "ClientId" },
        ]
      },
      {
        name: "PK_ClientRedirectUris",
        unique: true,
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
